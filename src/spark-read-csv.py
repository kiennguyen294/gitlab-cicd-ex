from pyspark.sql import SparkSession
import os


hdfs_path = os.environ['HDFS_PATH']

sparkSession = SparkSession.builder.appName("example-pyspark-read-and-write").getOrCreate()
df_load = sparkSession.read.csv(hdfs_path)
df_load.show()
