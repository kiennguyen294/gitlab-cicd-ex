FROM gcr.io/datamechanics/spark:platform-3.1-dm14

WORKDIR /opt
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY src/ .
CMD [ "python3", "spark-read-csv.py"]
#CMD ["sleep", "1d"]