## Cấu hình gitlab CI/CD với K8s
### 1. Mô hình triển khai

![architech](images/gitlab-cicd.png)

### 2. Setup Gitlab Server
#### Tạo project
* Trên dashboard chính của gitlab chọn *New project*

![new_project](images/newproject.png)

#### Thêm variables
* Trong *Settings* chọn *CI/CD* tab *Variables* và add các variables

![variables](images/variables.png)

### 3. Setup Gitlab Runner
* Trên VM cài đặt runner cài đặt runner bằng file binary
```
# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permissions to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab CI user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```
* Register runner

![register_runner](images/register_runner.png)

* Specific runners

Mặc định gitlab sẽ sử dụng shared runner nên cần disable shared runner 

![specific runner](images/specific_runners.png)

### 4. Viết Dockerfile
* Docker File
```
FROM python:3.8-slim-buster
WORKDIR /src
COPY ./requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY . .
ENV FLASK_APP=src/app
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]

```
### 5. Viết file gitlab-ce
* Gitlab-ce file
```
docker-build:
  # Use the official docker image.
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  # Default branch leaves tag empty (= latest tag)
  # All other branches are tagged with the escaped branch name (commit ref slug)
  script:
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag=""
        echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
      else
        tag=":$CI_COMMIT_REF_SLUG"
        echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
      fi
    - docker build --pull -t "$CI_REGISTRY_IMAGE${tag}" .
    - docker push "$CI_REGISTRY_IMAGE${tag}"
  # Run this job in a branch where a Dockerfile exists
deployment:
  stage: deploy
  script:
          #    - apt update
          #    - curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
          #    - chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl
          #    - kubectl config set-cluster k8s --server="${SERVER}"
          #    - kubectl config set clusters.k8s.certificate-authority-data ${CERTIFICATE_AUTHORITY_DATA}
          #    - kubectl config set-credentials gitlab --token="${USER_TOKEN}"
          #    - kubectl config view
    - kubectl apply -f ${HOME}/helloworld.yaml

```
### 6. Kiểm tra
* Trong CI/CD chọn Pipeline

![pipeline](images/pipeline.png)

* Kiểm tra trên cluster K8s

![cluster](images/cluster.png)
